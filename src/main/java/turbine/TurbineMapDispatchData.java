package turbine;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import turbine.model.DbContract;
import turbine.model.MapDispatchData;

/**
 * Root resource (exposed at "mapdispatch" path)
 */
@Path("mapdispatch")
public class TurbineMapDispatchData {

	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})	
	public Response getMapDispatchData() {
		
		String query = "select dp.settlementdate, dp.regionid, rrp, totaldemand, dispatchablegeneration, " + 
	             "availablegeneration, totalintermittentgeneration, netinterchange " +
			      "from   dispatchprice dp, dispatchregionsum drs, " +
			       "(select max(settlementdate) as maxdate " +
			              "from   dispatchprice) md " +
			      "where  dp.settlementdate = md.maxdate " + 
			      "and    dp.settlementdate = drs.settlementdate " + 
			      "and    dp.regionid = drs.regionid " +
			      "order by 1 desc;";
	
		List<MapDispatchData> people = new ArrayList<MapDispatchData>();

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(Connection connection = DriverManager.getConnection(DbContract.HOST + DbContract.DB_NAME,
				DbContract.USERNAME, DbContract.PASSWORD);
			Statement stmt = connection.createStatement();
		)
		{
			
			try(ResultSet rs = stmt.executeQuery(query)){
				
				while(rs.next()) {			
					MapDispatchData mapDispatchData = new MapDispatchData();
					
					mapDispatchData.setSettlementDate(rs.getDate(1));
					mapDispatchData.setRegionId(rs.getString(2));
					mapDispatchData.setRrp(new BigDecimal(rs.getDouble(3)));
					mapDispatchData.setTotalDemand(new BigDecimal(rs.getDouble(4)));					
					mapDispatchData.setDispatchableGeneration(new BigDecimal(rs.getDouble(5)));
					mapDispatchData.setAvailableGeneration(new BigDecimal(rs.getDouble(6)));
					mapDispatchData.setTotalIntermittentGeneration(new BigDecimal(rs.getDouble(7)));
					mapDispatchData.setNetInterchange(new BigDecimal(rs.getDouble(8)));
					
					people.add(mapDispatchData);	
				}	
			
			} 	
		} catch(Exception ex){
			
		}	
  	
		return Response.ok().entity(new GenericEntity<List<MapDispatchData>>(people){}).build();
	}
}
