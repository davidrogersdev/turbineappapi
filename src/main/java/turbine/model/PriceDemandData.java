package turbine.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PriceDemandData {

	private Date intervalDate;
	private String regionId;
	private BigDecimal rrp;
	private BigDecimal totalDemand;
	
	public Date getIntervalDate() {
		return intervalDate;
	}
	public void setIntervalDate(Date intervalDate) {
		this.intervalDate = intervalDate;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public BigDecimal getRrp() {
		return rrp;
	}
	public void setRrp(BigDecimal rrp) {
		this.rrp = rrp;
	}
	public BigDecimal getTotalDemand() {
		return totalDemand;
	}
	public void setTotalDemand(BigDecimal totalDemand) {
		this.totalDemand = totalDemand;
	}
	
}
