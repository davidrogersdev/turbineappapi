package turbine.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MapDispatchData {

	private BigDecimal availableGeneration; 
    private BigDecimal dispatchableGeneration; 
    private BigDecimal netInterchange;
    private String regionId;
    private BigDecimal rrp;
    private Date settlementDate; 
    private BigDecimal totalDemand; 
    private BigDecimal totalIntermittentGeneration;
    
	public BigDecimal getAvailableGeneration() {
		return availableGeneration;
	}
	public void setAvailableGeneration(BigDecimal availableGeneration) {
		this.availableGeneration = availableGeneration;
	}
	public BigDecimal getDispatchableGeneration() {
		return dispatchableGeneration;
	}
	public void setDispatchableGeneration(BigDecimal dispatchableGeneration) {
		this.dispatchableGeneration = dispatchableGeneration;
	}
	public BigDecimal getNetInterchange() {
		return netInterchange;
	}
	public void setNetInterchange(BigDecimal netInterchange) {
		this.netInterchange = netInterchange;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public BigDecimal getRrp() {
		return rrp;
	}
	public void setRrp(BigDecimal rrp) {
		this.rrp = rrp;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public BigDecimal getTotalDemand() {
		return totalDemand;
	}
	public void setTotalDemand(BigDecimal totalDemand) {
		this.totalDemand = totalDemand;
	}
	public BigDecimal getTotalIntermittentGeneration() {
		return totalIntermittentGeneration;
	}
	public void setTotalIntermittentGeneration(BigDecimal totalIntermittentGeneration) {
		this.totalIntermittentGeneration = totalIntermittentGeneration;
	}
    
    
}
