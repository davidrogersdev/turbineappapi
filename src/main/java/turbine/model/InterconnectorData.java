package turbine.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InterconnectorData {

	private BigDecimal exportLimit;
	private BigDecimal importLimit;
	private String interconnectorId;
	private BigDecimal mwFlow;
	private Date SettlementDate;
	
	public BigDecimal getExportLimit() {
		return exportLimit;
	}
	public void setExportLimit(BigDecimal exportLimit) {
		this.exportLimit = exportLimit.setScale(2,RoundingMode.HALF_UP);
	}
	public BigDecimal getImportLimit() {
		return importLimit;
	}
	public void setImportLimit(BigDecimal importLimit) {
		this.importLimit = importLimit.setScale(2, RoundingMode.HALF_UP);
	}
	public String getInterconnectorId() {
		return interconnectorId;
	}
	public void setInterconnectorId(String interconnectorId) {
		this.interconnectorId = interconnectorId;
	}
	public BigDecimal getMwFlow() {
		return mwFlow;
	}
	public void setMwFlow(BigDecimal mwFlow) {
		this.mwFlow = mwFlow.setScale(2, RoundingMode.HALF_UP);
	}
	public Date getSettlementDate() {
		return SettlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		SettlementDate = settlementDate;
	}
}
