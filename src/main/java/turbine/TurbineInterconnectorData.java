package turbine;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import turbine.model.DbContract;
import turbine.model.InterconnectorData;

/**
 * Root resource (exposed at "interconnector" path)
 */
@Path("interconnector")
public class TurbineInterconnectorData {

	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})	
	public Response getMapInterconnectorData() {
	
		String query = "select settlementdate, interconnectorid, exportlimit, importlimit, mwflow " + 
			      "from   dispatchinterconnectorres dicr, " +
			      "       (select max(settlementdate) as maxdate " + 
			      "        from   dispatchinterconnectorres) md " +
			      "where  dicr.settlementdate = md.maxdate " +
			      "and    dicr.lastchanged = (select max(lastchanged) " + 
			      "                           from   dispatchinterconnectorres " + 
			      "                           where  dicr.settlementdate = settlementdate) " + 
			      "order by 1 desc ";
	
	

		List<InterconnectorData> people = new ArrayList<InterconnectorData>();
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(Connection connection = DriverManager.getConnection(
				DbContract.HOST + DbContract.DB_NAME,
				DbContract.USERNAME, DbContract.PASSWORD);
			Statement stmt = connection.createStatement();
		){
			
			try(ResultSet rs = stmt.executeQuery(query)){
				
				
				while(rs.next()) {			
					InterconnectorData interconnectorData = new InterconnectorData();
					
					interconnectorData.setSettlementDate(rs.getDate(1));
					interconnectorData.setInterconnectorId(rs.getString(2));
					interconnectorData.setExportLimit(new BigDecimal(rs.getDouble(3)));
					interconnectorData.setImportLimit(new BigDecimal(rs.getDouble(4)));					
					interconnectorData.setMwFlow(new BigDecimal(rs.getDouble(5)));
					
					people.add(interconnectorData);	
				}
			}
		} catch(Exception ex){
			
		} 
		
		return Response.ok().entity(new GenericEntity<List<InterconnectorData>>(people){}).build();		
	}
}
