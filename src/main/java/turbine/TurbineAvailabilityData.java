package turbine;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import turbine.model.DbContract;
import turbine.model.PriceDemandData;

/**
 * Root resource (exposed at "availablegeneration" path)
 */
@Path("availablegeneration")
public class TurbineAvailabilityData {

	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})	
	public Response getAvailableGenerationData() {

		List<PriceDemandData> people = new ArrayList<PriceDemandData>();
		
		String query = "select interval_datetime as settlementdate, regionId, availableGeneration, dispatchableGeneration " + 
			       "from   P5MIN_REGIONSOLUTION pr, " +
			              "(select max(settlementdate) as maxdate " + 
			               "from   dispatchprice) md " +
			       "where  pr.interval_datetime > md.maxdate " +						       
			       "and    pr.lastchanged = (select max(lastchanged) " + 
			                                "from   P5MIN_REGIONSOLUTION " + 
			                                "where  regionid = pr.regionid " + 
			                                "and    pr.interval_datetime = interval_datetime) " + 
			       "order by 1 desc";
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(Connection connection = DriverManager.getConnection(
				DbContract.HOST + DbContract.DB_NAME,
				DbContract.USERNAME, DbContract.PASSWORD);
			Statement stmt = connection.createStatement();
		){
			
			try(ResultSet rs = stmt.executeQuery(query)){
				
				while(rs.next()) {			
					PriceDemandData priceDemandData = new PriceDemandData();
					
					priceDemandData.setIntervalDate(rs.getDate(1));
					priceDemandData.setRegionId(rs.getString(2));
					priceDemandData.setRrp(new BigDecimal(rs.getDouble(3)));
					priceDemandData.setTotalDemand(new BigDecimal(rs.getDouble(4)));
					
					people.add(priceDemandData);	
				}		
			}
		} catch(Exception ex){
			
		}					
			
		return Response.ok().entity(new GenericEntity<List<PriceDemandData>>(people){}).build();		
	}	
	
	@GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("getfootervalues")
	public Response getAvailableGenerationDataFooter() {

		String query = "select dp.settlementdate, dp.regionid, availableGeneration, dispatchableGeneration " +
			       "from   dispatchprice dp, dispatchregionsum drs " +
			       "where  dp.settlementdate = drs.settlementdate " +
			       "and    dp.regionid = drs.regionid " +       
			       "order by 1 desc "; 
		
		List<PriceDemandData> people = new ArrayList<PriceDemandData>();
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try(Connection connection = DriverManager.getConnection(
				DbContract.HOST + DbContract.DB_NAME,
				DbContract.USERNAME, DbContract.PASSWORD);
			Statement stmt = connection.createStatement();
		){
			
			try(ResultSet rs = stmt.executeQuery(query)){
				while(rs.next()) {			
					PriceDemandData priceDemandData = new PriceDemandData();
					
					priceDemandData.setIntervalDate(rs.getDate(1));
					priceDemandData.setRegionId(rs.getString(2));
					priceDemandData.setRrp(new BigDecimal(rs.getDouble(3)));
					priceDemandData.setTotalDemand(new BigDecimal(rs.getDouble(4)));
					
					people.add(priceDemandData);	
				}
								
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}    	
	
		return Response.ok().entity(new GenericEntity<List<PriceDemandData>>(people){}).build();		
	}		
}
